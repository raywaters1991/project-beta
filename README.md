# CarCar
CarCar is an application that can be used as an online interface for a car dealership. It will store and manage all information having to do with the dealership's inventories, sales, and services.

Team:

* Ray Waters - Sales
* Joshua Yu - Services

## How to get started
REQUREMENTS: You will need to have Docker, GIT, and Node.js 18.2 or above installed in order to use the application service.

You will need to first fork and clone the repo onto your local database. You can do this by  Then you will need to run some docker commands in this order: docker volume create beta-data, docker-compose build, and finally docker-compose-up. You can type http://localhost:3000/ into your browser in order to see the project on your browser.
## Design
CarCar uses three indivual microservices which are: sales, services, and inventory.

## Service microservice

The frontend portion of this microservice revolves around the forms and lists of services and technicians. The backend portion of this microservice uses the models to fill out the necessary information needed for the frontend portion of this application.


## Sales microservice

Explain your models and integration with the inventory
microservice, here.
