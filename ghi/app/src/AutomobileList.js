import React, {useEffect, useState } from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {

      const fetchAutomobiles = async () => {
        try {
          const response = await fetch('http://localhost:8100/api/automobiles/')
          const data = await response.json();
          console.log(data)
          setAutomobiles(data.autos);
        } catch (error) {
          console.error('Error fetching automobiles:', error);
        }
      };

      fetchAutomobiles();
    }, []);

    return (
        <div>
            <h2>Automobiles</h2>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
             <tbody>
                {automobiles.map(automobile => {
                    return (
                        <tr key={automobile.href}>
                            <td>{ automobile.vin }</td>
                            <td>{ automobile.color }</td>
                            <td>{ automobile.year }</td>
                            <td>{ automobile.model.name }</td>
                            <td>{ automobile.model.manufacturer.name }</td>
                            <td>{ automobile.sold ? 'Yes' : 'No'}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}
export default AutomobileList;
