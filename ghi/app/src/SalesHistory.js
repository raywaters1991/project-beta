import { useEffect, useState } from "react";

function SalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [filterSalespeople, setFilterSalespeople] = useState([]);
    const [sale , setSale] = useState([]);
    const [formData, setFormData] = useState({
        salesperson: '',

    });

    const getSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
            setFilterSalespeople(data.salespeople);
        }
    };

    useEffect(() => {

        getSalespeople();
        const fetchSale = async () => {
            const response = await fetch('http://localhost:8090/api/sales/');
            const data = await response.json();
            setSale(data.sale);
        };
        fetchSale();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();


    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if(salespeople.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }




    return (
        <>
        <div>
            <h2>Sales History</h2>
            <form>
            <select
            onChange={handleFormChange}
            value={formData.salesperson}
            required
            name="salesperson"
            id="salesperson"
            className={dropdownClasses}
        >
            <option value="">Select Sales Person</option>
            {salespeople.map(salesperson => (
                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                    {`${salesperson.first_name} ${salesperson.last_name} (${salesperson.employee_id})`}
                </option>
            ))}
        </select>

            </form>
            <table className="table table-striped-columns">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Vin</th>
                        <th>Customer</th>
                        <th>price</th>

                    </tr>
                </thead>
                <tbody>
                    {filterSalespeople.map((salesperson) => {
                        return (
                            <tr key={salesperson.employee_id}>
                                <td>{salesperson.name}</td>

                                <td>{salesperson.customer}</td>
                                <td>{salesperson.vin}</td>
                                <td>{salesperson.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    );
}}
export default SalesHistory;
