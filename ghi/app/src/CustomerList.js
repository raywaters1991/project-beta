import React, { useState, useEffect } from 'react';

function CustomerList() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    const fetchCustomers = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/customers/');
        const data = await response.json();

        setCustomers(data.customers);
      } catch (error) {
        console.error('Error fetching customers:', error);
      }
    };

    fetchCustomers();
  }, []);

  return (
    <div>
      <h2>Customers</h2>

    <table className="table table-striped-columns">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Address</th>
          <th>Phone Number</th>

        </tr>
      </thead>
      <tbody>
        {customers.map(customer => (
          <tr key={customer.id}>
            <td>{customer.first_name}</td>
            <td>{customer.last_name}</td>
            <td>{customer.address}</td>
            <td>{customer.phone_number}</td>

          </tr>
        ))}
      </tbody>
    </table>
    </div>
  );
}

export default CustomerList;
