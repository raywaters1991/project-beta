import { useEffect, useState } from "react";

function TechnicianForm() {
    const [technician, setTechnician] = useState('');
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();


        const url = 'http://localhost:8080/api/technicians/';

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();
    if (response.ok) {
        setFormData({
            first_name: '',
            last_name: '',
            employee_id: '',
        });
            console.log('Technician created successfully');
        } else {
            console.error('Failed to create technician');
        }
        } catch (error) {
            console.error('Error creating technician', error);
        }
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a technician</h1>
                        <form onSubmit={handleSubmit} id="add-technician-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.first_name}
                                    placeholder="First_name"
                                    required
                                    type="text"
                                    name="first_name"
                                    id="first_name"
                                    className="form-control"
                                />
                                <label htmlFor="name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.last_name}
                                    placeholder="Last_name"
                                    required
                                    type="text"
                                    name="last_name"
                                    id="last_name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.employee_id}
                                    placeholder="Employee_id"
                                    required
                                    type="text"
                                    name="employee_id"
                                    id="employee_id"
                                    className="form-control"
                                />
                                <label htmlFor="name">Employee ID</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default TechnicianForm;
