from django.urls import path

from .views import (
    api_sales,
    api_delete_sale,
    api_customers,
    api_delete_customer,
    api_salespeople,
    api_delete_salesperson,
)

urlpatterns = [
    path(
        "salespeople/",
        api_salespeople,
        name="api_salespeople",
    ),
    path(
        "salespeople/<int:pk>/",
        api_delete_salesperson,
        name="api_delete_salesperson",
    ),
    path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "customers/<int:pk>/",
        api_delete_customer,
        name="api_delete_customer",
    ),
    path(
        "sales/",
        api_sales,
        name="api_sales",
    ),
    path(
        "sales/<int:pk>/",
        api_delete_sale,
        name="api_delete_sale",
    ),
]
